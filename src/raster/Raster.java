package raster;

public interface Raster<V> {
    int getWidth();
    int getHeight();

    V getElement(int x, int y);
    void setElement(int x, int y, V value);

    void clear();
    void setClearValue(V value);

    default boolean checkBorders(int x, int y){
        //TODO
        return true;
    }
}
