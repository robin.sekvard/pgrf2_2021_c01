package raster;

import transforms.Col;

public class ZbufferVisibility {
    private ImageBuffer iBuffer;
    private DepthBuffer zBuffer;

    public ZbufferVisibility(ImageBuffer imageBuffer) {
        iBuffer = imageBuffer;
        zBuffer = new DepthBuffer(imageBuffer.getWidth(),imageBuffer.getHeight());
    }

    //TODO konstruktor

    //TODO clear

    public void drawPixelWithZtest(int x, int y, double z, Col color){
        iBuffer.setElement(x,y,color);
    }

}
